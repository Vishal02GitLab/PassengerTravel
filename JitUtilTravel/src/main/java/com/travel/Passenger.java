package com.travel;

public abstract class Passenger

{
	public double charge;
	public double rateFactor =  0.5;
	
	public abstract  double calculateFare();

	@Override
	public String toString() {
		return "Passenger [charge=" + charge + ", rateFactor=" + rateFactor + "]";
	}
		
		
		

}
