package com.travel;

public class Commuter extends Passenger {

	
	
	private int noOfStops;
	private boolean isRiderCard;
	
	@Override
	public double calculateFare() {
		
		charge = rateFactor * noOfStops;
		
		if(isRiderCard) {
			charge = charge - 0.1 * charge;
		}
		return charge;
	}

	public int getNoOfStops() {
		return noOfStops;
	}

	public void setNoOfStops(int noOfStops) {
		this.noOfStops = noOfStops;
	}

	public boolean isRiderCard() {
		return isRiderCard;
	}

	public void setRiderCard(boolean isRiderCard) {
		this.isRiderCard = isRiderCard;
	}

}
