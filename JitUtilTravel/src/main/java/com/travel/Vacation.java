package com.travel;

public class Vacation extends Passenger implements Meal{

	private int noOfMiles;
	private int noOfMeals =0;

	public int getNoOfMiles() {
		return noOfMiles;
	}

	public void setNoOfMiles(int noOfMiles) {
		this.noOfMiles = noOfMiles;
	}
	

	public int getNoOfMeals() {
		return noOfMeals;
	}

	public void setNoOfMeals(int noOfMeals) {
		this.noOfMeals = noOfMeals;
	}

	@Override
	public double calculateFare() {
		if(noOfMiles <5 || noOfMiles>4000) {
			throw new IllegalArgumentException("No of Miles not valid");
		}
		else {
			charge = rateFactor * noOfMiles;
			
		}
		
		return charge;
	}

	public int calculateMeal() {
		//if (noOfMiles >=100) {
			//noOfMeals = Math.round(noOfMiles / 100);
		noOfMeals = (noOfMiles / 100);
		if(noOfMiles%100 >0 && noOfMiles%100 <100) {
			noOfMeals = noOfMeals+1;
		}
		//}
			
		return noOfMeals;
	}
	
	
	
}
