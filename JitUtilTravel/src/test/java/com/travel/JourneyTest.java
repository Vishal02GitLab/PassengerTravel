package com.travel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class JourneyTest {

	Commuter comPassenger;
	Vacation vacPassenger; 
	
	@Before
	public void doSetUp() {
		comPassenger = new Commuter();
		vacPassenger = new Vacation();
	}
	@Test
	public void testCommuterCost() {
		
		
		
		comPassenger.setNoOfStops(4);;
		int expected = 2;
		double actual  = comPassenger.calculateFare();
		System.out.println("testCommuterCost "+ actual);
		assertEquals(expected, actual,.001);
	}
	
	@Test
	public void testVacationCost() {
		vacPassenger.setNoOfMiles(199);
		double expected = 99.5;
		double actual = vacPassenger.calculateFare();
		System.out.println("testVacationCost " +actual);
		assertEquals(expected, actual,.001);
	}
	
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Test
	public void testMilesEstimation() {
		anException.expect(IllegalArgumentException.class);
		anException.expectMessage("No of Miles not valid");
		vacPassenger.setNoOfMiles(4);
		Double actual = vacPassenger.calculateFare();
		System.out.println("testMilesEstimation " + actual);
		
	}
	
	@Test
	public void testMealCalculation() {
		vacPassenger.setNoOfMiles(201);
		int expected = 3;
		int actual = vacPassenger.calculateMeal();
		System.out.println("testMealCalculation: " + actual);
		assertEquals(expected, actual,.001);
		
	}
	
	

	
}
